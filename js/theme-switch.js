

toggleTheme = function() {
  if(document.getElementById('color-theme').getAttribute("href") === '/css/colors/light.css'){
    setTheme("dark", "light")
      }
  else{
    setTheme("light", "dark")
}};


setTheme = function(newTheme, oldTheme) {
    document.getElementById('color-theme').href = '/css/colors/' +   newTheme + '.css';
    document.getElementById('theme-switcher').innerHTML = "Switch to " + oldTheme + " theme?";
    if(docCookies.hasItem("theme")){
      docCookies.removeItem("theme");
    }
    docCookies.setItem("theme", newTheme, Infinity, "/", "nemmy.eerie.garden");
};

if(docCookies.hasItem("theme") && docCookies.getItem("theme") === 'light'){
  setTheme("light", "dark");
}
else if(docCookies.hasItem("theme") && docCookies.getItem("theme") === 'dark'){
  setTheme("dark", "light");
}
else if(window.matchMedia('(prefers-color-scheme: dark)').matches){
  setTheme("dark", "light");
}
else if(window.matchMedia('(prefers-color-scheme: light)').matches){
  setTheme("light", "dark");
};
